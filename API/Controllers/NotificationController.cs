﻿using System;
using System.Threading.Tasks;
using API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly IHubContext<ChatHub> _notificationUserHubContext;
        private readonly IUserConnectionManager _userConnectionManager;

        public NotificationController(IHubContext<ChatHub> notificationUserHubContext, IUserConnectionManager userConnectionManager)
        {
            _notificationUserHubContext = notificationUserHubContext;
            _userConnectionManager = userConnectionManager;
        }


        // GET: api/values
        [HttpGet("SendMessage")]
        public async Task<IActionResult> SendMessageAsync(string id, string title, string content)
        {
            try
            {
                var connections = _userConnectionManager.GetUserConnections(id);
                Console.WriteLine(connections.Count);
                if (connections != null && connections.Count > 0)
                {
                    foreach (var connectionId in connections)
                    {
                        await _notificationUserHubContext.Clients.Client(connectionId).SendAsync("sendToUser", title, content);
                    }
                }
                return Ok();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return BadRequest();
        }
    }
}
