﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace API.Models
{
    public class ChatHub : Hub
    {
        private readonly IUserConnectionManager _userConnectionManager;
        public ChatHub(IUserConnectionManager userConnectionManager)
        {
            _userConnectionManager = userConnectionManager;
        }
        public override Task OnConnectedAsync()
        {
            var httpContext = this.Context.GetHttpContext();
            var userId = httpContext.Request.Query["userId"];
            _userConnectionManager.KeepUserConnection(userId, Context.ConnectionId);
            Console.WriteLine("userId => " + userId);
            Console.WriteLine("Context.ConnectionId => " + Context.ConnectionId);
            return base.OnConnectedAsync();
        }
        public string GetConnectionId()
        {
            var httpContext = this.Context.GetHttpContext();
            var userId = httpContext.Request.Query["userId"];
            _userConnectionManager.KeepUserConnection(userId, Context.ConnectionId);
            Console.WriteLine("userId => " + userId);
            Console.WriteLine("Context.ConnectionId => " + Context.ConnectionId);
            return Context.ConnectionId;
        }
        public async override Task OnDisconnectedAsync(Exception exception)
        {
            //get the connectionId
            var connectionId = Context.ConnectionId;
            _userConnectionManager.RemoveUserConnection(connectionId);
            var value = await Task.FromResult(0);//adding dump code to follow the template of Hub > OnDisconnectedAsync
        }
    }
}
