﻿using System;
namespace API.Models
{
    public class UMessage
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
